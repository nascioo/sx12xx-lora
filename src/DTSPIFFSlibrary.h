/*******************************************************************************************************
  Programs for Arduino - Copyright of the author Stuart Robinson - 08/11/21

  This code is supplied as is, it is up to the user of the program to decide if the program is suitable
  for the intended purpose and free from errors.
*******************************************************************************************************/


#include "SPIFFS.h" 
File dataFile;    


bool DTSPIFFS_dumpFileASCII(char *buff);
bool DTSPIFFS_dumpFileHEX(char *buff);
bool DTSPIFFS_dumpSegmentHEX(uint8_t segmentsize);
bool DTSPIFFS_initSPIFFS();
uint32_t DTSPIFFS_getFileSize(char *buff);
void DTSPIFFS_printDirectory();
uint32_t DTSPIFFS_openFileRead(char *buff);
uint16_t DTSPIFFS_getNumberSegments(uint32_t filesize, uint8_t segmentsize);
uint8_t DTSPIFFS_getLastSegmentSize(uint32_t filesize, uint8_t segmentsize);
bool DTSPIFFS_openNewFileWrite(char *buff);
bool DTSPIFFS_openFileWrite(char *buff, uint32_t position);
uint8_t DTSPIFFS_readFileSegment(uint8_t *buff, uint8_t segmentsize);
uint8_t DTSPIFFS_writeSegmentFile(uint8_t *buff, uint8_t segmentsize);
void DTSPIFFS_seekFileLocation(uint32_t position);
uint16_t DTSPIFFS_createFile(char *buff);
uint16_t DTSPIFFS_fileCRCCCITT();
void DTSPIFFS_fileFlush();
void DTSPIFFS_closeFile();
void printDirectorySPIFFS(File dir, int numTabs);


bool DTSPIFFS_dumpFileASCII(char *buff)
{

  File dataFile = SPIFFS.open(buff);                  //open the test file note that only one file can be open at a time,

  if (dataFile)                                   //if the file is available, read from it
  {
    while (dataFile.available())
    {
      Serial.write(dataFile.read());
    }
    dataFile.close();
    return true;
  }
  else
  {
    return false;
  }
}


bool DTSPIFFS_dumpFileHEX(char *buff)
{
  //Note, this function will return true if the SPIFFS card is removed.
  uint16_t Loopv1, Loopv2;
  uint8_t fileData;
  uint32_t filesize;

  if (!SPIFFS.exists(buff))
  {
    return false;
  }

  File dataFile = SPIFFS.open(buff);
  filesize = dataFile.size();
  filesize--;                                      //file data locations are from 0 to (filesize -1);
  Serial.print(F("Lcn    0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F"));
  Serial.println();

  if (dataFile)                                    //if the file is available, read from it
  {
    while (dataFile.available())
    {
      for (Loopv1 = 0; Loopv1 <= filesize;)
      {
        Serial.print(F("0x"));
        if (Loopv1 < 0x10)
        {
          Serial.print(F("0"));
        }
        Serial.print((Loopv1), HEX);
        Serial.print(F("  "));
        for (Loopv2 = 0; Loopv2 <= 15; Loopv2++)
        {
          fileData = dataFile.read();
          if (fileData < 0x10)
          {
            Serial.print(F("0"));
          }
          Serial.print(fileData, HEX);
          Serial.print(F(" "));
          Loopv1++;
        }
        Serial.println();
      }
    }
    dataFile.close();
    return true;
  }
  else
  {
    Serial.println(F("File not available"));
    return false;
  }
}


bool DTSPIFFS_initSPIFFS()
{
  if (SPIFFS.begin())
  {
    return true;
  }
  else
  {
    return false;
  }
}


uint32_t DTSPIFFS_getFileSize(char *buff)
{
  uint32_t filesize;

  if (!SPIFFS.exists(buff))
  {
    return 0;
  }

  dataFile = SPIFFS.open(buff);
  filesize = dataFile.size();
  dataFile.close();
  return filesize;
}


#ifdef SPIFFSFATLIB
void DTSPIFFS_printDirectory()
{
  dataFile = SPIFFS.open("/");
  Serial.println(F("Card directory"));
  SPIFFS.ls("/", LS_R);
}
#endif


#ifdef SPIFFSLIB
void DTSPIFFS_printDirectory()
{
  dataFile = SPIFFS.open("/");
  
  Serial.println();
}


void printDirectorySPIFFS(File dir, int numTabs)
{

 
    File entry =  dir.openNextFile();
}
#endif


bool DTSPIFFS_dumpSegmentHEX(uint8_t segmentsize)
{
  uint16_t Loopv1, Loopv2;
  uint8_t fileData;

  Serial.print(F("Print segment of "));
  Serial.print(segmentsize);
  Serial.println(F(" bytes"));
  Serial.print(F("Lcn    0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F"));
  Serial.println();

  if (dataFile)                                    //if the file is available, read from it
  {
    for (Loopv1 = 0; Loopv1 < segmentsize;)
    {
      Serial.print(F("0x"));
      if (Loopv1 < 0x10)
      {
        Serial.print(F("0"));
      }
      Serial.print((Loopv1), HEX);
      Serial.print(F("  "));
      for (Loopv2 = 0; Loopv2 <= 15; Loopv2++)
      {
        //stop printing if all of segment has been printed
        if (Loopv1 < segmentsize)
        {
          fileData = dataFile.read();
          if (fileData < 0x10)
          {
            Serial.print(F("0"));
          }
          Serial.print(fileData, HEX);
          Serial.print(F(" "));
          Loopv1++;
        }
      }
      Serial.println();
    }
    return true;
  }
  else
  {
    return false;
  }
}


uint32_t DTSPIFFS_openFileRead2(char *buff)
{
  uint32_t filesize;

  dataFile = SPIFFS.open(buff);
  filesize = dataFile.size();
  dataFile.seek(0);
  return filesize;
}


uint32_t DTSPIFFS_openFileRead(char *buff)
{
  uint32_t filesize;
  
  if (SPIFFS.exists(buff))
  {
  //Serial.println(F("File exists"));
  dataFile = SPIFFS.open(buff);
  filesize = dataFile.size();
  dataFile.seek(0);
  return filesize;
  }
  else
  {
   //Serial.println(F("File does not exist"));
   return 0;
  }
}

 
uint16_t DTSPIFFS_getNumberSegments(uint32_t filesize, uint8_t segmentsize)
{
  uint16_t segments;
  segments = filesize / segmentsize;

  if ((filesize % segmentsize) > 0)
  {
    segments++;
  }
  return segments;
}


uint8_t DTSPIFFS_getLastSegmentSize(uint32_t filesize, uint8_t segmentsize)
{
  uint8_t lastsize;

  lastsize = filesize % segmentsize;
  if (lastsize == 0)
  {
    lastsize = segmentsize;
  }
  return lastsize;
}


bool DTSPIFFS_openNewFileWrite(char *buff)
{
  if (SPIFFS.exists(buff))
  {
    //Serial.print(buff);
    //Serial.println(F(" File exists - deleting"));
    SPIFFS.remove(buff);
  }

  if (dataFile = SPIFFS.open(buff, FILE_WRITE))
  {
    //Serial.print(buff);
    //Serial.println(F(" SPIFFS File opened"));
    return true;
  }
  else
  {
    //Serial.print(buff);
    //Serial.println(F(" ERROR opening file"));
    return false;
  }
}


bool DTSPIFFS_openFileWrite(char *buff, uint32_t position)
{
  dataFile = SPIFFS.open(buff, FILE_WRITE);   //seems to operate as append
  dataFile.seek(position);                //seek to first position in file

  if (dataFile)
  {
    return true;
  }
  else
  {
    return false;
  }
}


uint8_t DTSPIFFS_readFileSegment(uint8_t *buff, uint8_t segmentsize)
{
  uint8_t index = 0;
  uint8_t fileData;

  while (index < segmentsize)
  {
    fileData = (uint8_t) dataFile.read();
    buff[index] = fileData;
    index++;
  };

  if (index == segmentsize)
  {
    return segmentsize;            //if all written return segment size
  }
  else
  {
    return index - 1;               //if not all written return number bytes read
  }
}


uint8_t DTSPIFFS_writeSegmentFile(uint8_t *buff, uint8_t segmentsize)
{
  uint8_t index, byteswritten = 0;

  for (index = 0; index < segmentsize; index++)
  {
    dataFile.write(buff[index]);
    byteswritten++;
  }
  return byteswritten;
}


void DTSPIFFS_seekFileLocation(uint32_t position)
{
  dataFile.seek(position);                       //seek to position in file
  return;
}


uint16_t DTSPIFFS_createFile(char *buff)
{
  //creats a new filename use this definition as the base;
  //char filename[] = "/SPIFFS0000.txt";     //filename used as base for creating logfile, 0000 replaced with numbers
  //the 0000 in the filename is replaced with the next number avaialable

  uint16_t index;

  for (index = 1; index <= 9999; index++) {
    buff[3] = index / 1000 + '0';
    buff[4] = ((index % 1000) / 100) + '0';
    buff[5] = ((index % 100) / 10) + '0';
    buff[6] = index % 10 + '0' ;
    if (! SPIFFS.exists(buff))
    {
      // only open a new file if it doesn't exist
      dataFile = SPIFFS.open(buff, FILE_WRITE);
      break;
    }
  }

  if (!dataFile)
  {
    return 0;
  }

  return index;                                      //return number of logfile created
}


uint16_t DTSPIFFS_fileCRCCCITT(uint32_t fsize)
{
  uint32_t index;
  uint16_t CRCcalc;
  uint8_t j, filedata;

  CRCcalc = 0xFFFF;                                  //start value for CRC16

  for (index = 0; index < fsize; index++)
  {
    filedata = dataFile.read();
    CRCcalc ^= (((uint16_t) filedata ) << 8);
    for (j = 0; j < 8; j++)
    {
      if (CRCcalc & 0x8000)
        CRCcalc = (CRCcalc << 1) ^ 0x1021;
      else
        CRCcalc <<= 1;
    }
  }

  return CRCcalc;
}


void DTSPIFFS_fileFlush()
{
  dataFile.flush();
}


void DTSPIFFS_closeFile()
{
    if(!dataFile)
  {
    Serial.println("errore salvataggio!");
  }
  dataFile.close();

                                //close local file
}
