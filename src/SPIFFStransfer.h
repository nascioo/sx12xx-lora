/*******************************************************************************************************
  Programs for Arduino - Copyright of the author Stuart Robinson - 13/01/22

  The functions expect the calling sketch to create an instance called LoRa, so that functions
  are called like this; LoRa.getSPIFFSTXNetworkID().

  This code is supplied as is, it is up to the user of the program to decide if the program is suitable
  for the intended purpose and free from errors.

  There is a copy of this file in the SX12XX-LoRa library \src folder, but the file can be copied to the
  sketch folder and used locally. In this way its possible to carry out custom modifications.

*******************************************************************************************************/

//110122 added local function printArrayHEX(uint8_t *buff, uint32_t len)
//130122 Made variable and function names unique so that the array transfer routines can be used in the same program
//130122 Converted all Serial prints to Monitorport.print() format

#define SPIFFSUNUSED(v) (void) (v)               //add SPIFFSUNUSED(variable); to avoid compiler warnings 


#ifndef Monitorport
#define Monitorport Serial
#endif

#include <arrayRW.h>
//#define DEBUG                              //enable this define to print debug info for segment transfers

//Variables used on tranmitter and receiver
uint8_t SPIFFSRXPacketL;                         //length of received packet
uint8_t SPIFFSRXPacketType;                      //type of received packet, segment write, ACK, NACK etc
uint8_t SPIFFSRXHeaderL;                         //length of header
int16_t SPIFFSPacketRSSI;                        //stores RSSI of received packet
int8_t  SPIFFSPacketSNR;                         //stores signal to noise ratio of received packet
uint16_t SPIFFSAckCount;                         //keep a count of acks that are received within timeout period
uint16_t SPIFFSNoAckCount;                       //keep a count of acks not received within timeout period
uint16_t SPIFFSDTDestinationFileCRC;             //CRC of complete file received
uint32_t SPIFFSDTDestinationFileLength;          //length of file written on the destination\receiver
uint16_t SPIFFSDTSourceFileCRC;                  //CRC returned of the remote saved file
uint32_t SPIFFSDTSourceFileLength;               //length of file at source\transmitter
uint32_t SPIFFSDTStartmS;                        //used for timeing transfers
uint16_t SPIFFSDTSegment = 0;                      //current segment number
char SPIFFSDTfilenamebuff[Maxfilenamesize];       //global buffer to store current filename
uint8_t SPIFFSDTheader[16];                      //header array
uint8_t SPIFFSDTdata[245];                       //data/segment array
uint8_t SPIFFSDTflags = 0;                       //Flags byte used to pass status information between nodes
int SPIFFSDTLED = -1;                            //pin number for indicator LED, if -1 then not used
uint16_t SPIFFSDTErrors;                         //used for tracking errors in the transfer process

//Transmitter mode only variables
uint16_t SPIFFSTXNetworkID;                      //this is used to store the 'network' number, receiver must have the same networkID
uint16_t SPIFFSTXArrayCRC;                       //should contain CRC of data array transmitted
uint8_t  SPIFFSTXPacketL;                        //length of transmitted packet
uint16_t SPIFFSLocalPayloadCRC;                  //for calculating the local data array CRC
uint8_t SPIFFSDTLastSegmentSize;                 //size of the last segment
uint16_t SPIFFSDTNumberSegments;                 //number of segments for a file transfer
uint16_t SPIFFSDTSentSegments;                   //count of segments sent
bool SPIFFSDTFileTransferComplete;               //bool to flag file transfer complete
uint32_t SPIFFSDTSendmS;                         //used for timing transfers
float SPIFFSDTsendSecs;                          //seconds to transfer a file


//Receive mode only variables
uint16_t SPIFFSRXErrors;                         //count of packets received with error
uint8_t SPIFFSRXFlags;                           //SPIFFSDTflags byte in header, could be used to control actions in TX and RX
uint8_t SPIFFSRXDataarrayL;                      //length of data array\segment
bool SPIFFSDTFileOpened;                         //bool to flag when file has been opened
uint16_t SPIFFSDTSegmentNext;                    //next segment expected
uint16_t SPIFFSDTReceivedSegments;               //count of segments received
uint16_t SPIFFSDTSegmentLast;                    //last segment processed


//Transmitter mode functions
uint32_t SPIFFSsendFile(char *filename, uint8_t namelength);
bool SPIFFSstartFileTransfer(char *filename, uint8_t filenamesize);
bool SPIFFSsendSegments();
bool SPIFFSsendFileSegment(uint16_t segnum, uint8_t segmentsize);
bool SPIFFSendFileTransfer(char *filename, uint8_t filenamesize);
void SPIFFSbuild_DTFileOpenHeader(uint8_t *header, uint8_t headersize, uint8_t datalength, uint32_t filelength, uint16_t filecrc, uint8_t segsize);
void SPIFFSbuild_DTSegmentHeader(uint8_t *header, uint8_t headersize, uint8_t datalen, uint16_t segnum);
void SPIFFSbuild_DTFileCloseHeader(uint8_t *header, uint8_t headersize, uint8_t datalength, uint32_t filelength, uint16_t filecrc, uint8_t segsize);
void SPIFFSprintLocalFileDetails();
void SPIFFSprintSeconds();
void SPIFFSprintAckBrief();
void SPIFFSprintAckReception();
void SPIFFSprintACKdetail();
void SPIFFSprintdata(uint8_t *dataarray, uint8_t arraysize);
void SPIFFSprintPacketHex();
bool SPIFFSsendDTInfo();
void SPIFFSbuild_DTInfoHeader(uint8_t *header, uint8_t headersize, uint8_t datalen);

//Receiver mode functions
bool SPIFFSreceiveaPacketDT();
void SPIFFSreadHeaderDT();
bool SPIFFSprocessPacket(uint8_t packettype);
void SPIFFSprintPacketDetails();
bool SPIFFSprocessSegmentWrite();
bool SPIFFSprocessFileOpen(uint8_t *filename, uint8_t filenamesize);
bool SPIFFSprocessFileClose();
void SPIFFSprintPacketRSSI();
void SPIFFSprintSourceFileDetails();
void SPIFFSprintDestinationFileDetails();

//Common functions
void SPIFFSsetLED(int8_t pinnumber);
void SPIFFSprintheader(uint8_t *header, uint8_t headersize);
void SPIFFSprintReliableStatus();

//bit numbers used by SPIFFSDTErrors (16bits) and RXErrors (first 8bits)
const uint8_t SPIFFSNoFileSave = 0;              //bit number of SPIFFSDTErrors to set when no file save, to SPIFFS for example
const uint8_t SPIFFSNothingToSend = 1;           //bit number of SPIFFSDTErrors to set when nothing to send or unable to send image\file
const uint8_t SPIFFSNoCamera = 1;                //bit number of SPIFFSDTErrors to set when camera fails
const uint8_t SPIFFSSendFile = 2;                //bit number of SPIFFSDTErrors to set when file SPIFFS file image\file send fail
const uint8_t SPIFFSSendArray = 2;               //bit number of SPIFFSDTErrors to set when file array image\file send fail
const uint8_t SPIFFSNoACKlimit = 3;              //bit number of SPIFFSDTErrors to set when NoACK limit reached
const uint8_t SPIFFSSendPacket = 4;              //bit number of SPIFFSDTErrors to set when sending a packet fails or there is no ack

const uint8_t SPIFFSStartTransfer = 11;          //bit number of SPIFFSDTErrors to set when StartTransfer fails
const uint8_t SPIFFSSendSegments = 12;           //bit number of SPIFFSDTErrors to set when SendSegments function fails
const uint8_t SPIFFSSendSegment = 13;            //bit number of SPIFFSDTErrors to set when sending a single Segment send fails
const uint8_t SPIFFSOpeningFile = 14;            //bit number of SPIFFSDTErrors to set when opening file fails
const uint8_t SPIFFSendTransfer = 15;            //bit number of SPIFFSDTErrors to set when end transfer fails



//************************************************
//Transmit mode functions
//************************************************

uint32_t SPIFFSsendFile(char *filename, uint8_t namelength)
{
  // This routine allows the file transfer to be run with a function call of sendFile(filename, sizeof(filename));
  memcpy(SPIFFSDTfilenamebuff, filename, namelength);  //copy the name of file into global filename array for use outside this function

  uint8_t localattempts = 0;
  
  SPIFFSDTErrors = 0;                                  //clear all error flags
  SPIFFSDTDestinationFileCRC = 0;
  SPIFFSDTSourceFileCRC = 0;
  SPIFFSDTDestinationFileLength = 0;
  SPIFFSDTSourceFileLength = 0;

  do
  {
    localattempts++;
    SPIFFSNoAckCount = 0;
    SPIFFSDTStartmS = millis();

    Monitorport.print(F("Send file attempt "));
    Monitorport.println(localattempts);

    //opens the local file to send and sets up transfer parameters
    if (SPIFFSstartFileTransfer(filename, namelength))
    {
#ifdef ENABLEMONITOR
      Monitorport.print(filename);
      Monitorport.println(F(" opened OK on remote"));
      SPIFFSprintLocalFileDetails();
#endif
    }
    else
    {
#ifdef ENABLEMONITOR
      Monitorport.println(F("********************"));
      Monitorport.println(filename);
      Monitorport.println(F("ERROR opening file"));
      Monitorport.println(F("Restarting transfer"));
      Monitorport.println(F("********************"));
#endif
      SPIFFSDTFileTransferComplete = false;
      delay(2000);
      continue;
    }

    delay(FunctionDelaymS);

    if (!SPIFFSsendSegments())
    {
#ifdef ENABLEMONITOR
      Monitorport.println();
      Monitorport.println(F("************************"));
      Monitorport.println(filename);
      Monitorport.println(F("ERROR in SPIFFSsendSegments()"));
      Monitorport.println(F("Restarting transfer"));
      Monitorport.println(F("***********************"));
      Monitorport.println();
#endif
      SPIFFSDTFileTransferComplete = false;
      continue;
    }

    delay(FunctionDelaymS);

    if (SPIFFSendFileTransfer(filename, namelength))             //send command to close remote file
    {
      SPIFFSDTSendmS = millis() - SPIFFSDTStartmS;                   //record time taken for transfer
      beginarrayRW(SPIFFSDTheader, 4);
      SPIFFSDTDestinationFileLength = arrayReadUint32();
#ifdef ENABLEMONITOR
      Monitorport.print(filename);
      Monitorport.println(F(" closed OK on remote"));
      Monitorport.print(F("Acknowledged remote destination file length "));
      Monitorport.println(SPIFFSDTDestinationFileLength);
#endif

      if (SPIFFSDTDestinationFileLength != SPIFFSDTSourceFileLength)
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("*******************************"));
        Monitorport.println(filename);
        Monitorport.println(F("ERROR file lengths do not match"));
        Monitorport.println(F("Restarting transfer"));
        Monitorport.println(F("*******************************"));
#endif
        SPIFFSDTFileTransferComplete = false;
        continue;
      }
      else
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("File lengths match"));
#endif
      }
#ifdef ENABLEFILECRC

      SPIFFSDTDestinationFileCRC = arrayReadUint16();
#ifdef ENABLEMONITOR
      Monitorport.print(F("Acknowledged remote destination file CRC 0x"));
      Monitorport.println(SPIFFSDTDestinationFileCRC, HEX);
#endif

      if (SPIFFSDTDestinationFileCRC != SPIFFSDTSourceFileCRC)
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("****************************"));
        Monitorport.println(filename);
        Monitorport.println(F("ERROR file CRCs do not match"));
        Monitorport.println(F("Restarting transfer"));
        Monitorport.println(F("****************************"));
#endif
        SPIFFSDTFileTransferComplete = false;
        continue;
      }
      else
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("File CRCs match"));
#endif
      }
#endif   //end of ENABLEFILECRC
      SPIFFSDTFileTransferComplete = true;
    }
    else
    {
#ifdef ENABLEMONITOR
      Monitorport.println(F("******************************"));
      Monitorport.println(filename);
      Monitorport.println(F("ERROR close remote file failed"));
      Monitorport.println(F("Restarting transfer"));
      Monitorport.println(F("******************************"));
#endif
      SPIFFSDTFileTransferComplete = false;
      continue;
    }
  }
  while ((!SPIFFSDTFileTransferComplete) && (localattempts < StartAttempts));

  SPIFFSDTsendSecs = (float) SPIFFSDTSendmS / 1000;

#ifdef ENABLEMONITOR
  Monitorport.print(F("StartAttempts "));
  Monitorport.println(localattempts);
  Monitorport.print(F("SPIFFSNoAckCount "));
  Monitorport.println(SPIFFSNoAckCount);
  Monitorport.print(F("Transmit time "));
  Monitorport.print(SPIFFSDTsendSecs, 3);
  Monitorport.println(F("secs"));
  Monitorport.print(F("Transmit rate "));
  Monitorport.print( (SPIFFSDTDestinationFileLength * 8) / (SPIFFSDTsendSecs), 0 );
  Monitorport.println(F("bps"));
#endif

  if (localattempts == StartAttempts)
  {
    bitSet(SPIFFSDTErrors, SPIFFSSendFile);
    return 0;
  }

  return SPIFFSDTDestinationFileLength;
}


bool SPIFFSstartFileTransfer(char *filename, uint8_t filenamesize)
{
  //Start file transfer, open local file first then remote file.

  uint8_t ValidACK;
  uint8_t localattempts = 0;

#ifdef ENABLEMONITOR
  Monitorport.print(F("Start file transfer for "));
  Monitorport.println(filename);
#endif
  SPIFFSDTSourceFileLength = DTSPIFFS_openFileRead(filename);                   //get the file length

  if (SPIFFSDTSourceFileLength == 0)
  {
#ifdef ENABLEMONITOR
    Monitorport.print(F("Error - opening file"));
    Monitorport.println(filename);
#endif
    bitSet(SPIFFSDTErrors, SPIFFSOpeningFile);
    return false;
  }

#ifdef ENABLEFILECRC
  SPIFFSDTSourceFileCRC = DTSPIFFS_fileCRCCCITT(SPIFFSDTSourceFileLength);        //get file CRC from position 0 to end
#endif

  SPIFFSDTNumberSegments = DTSPIFFS_getNumberSegments(SPIFFSDTSourceFileLength, SegmentSize);
  SPIFFSDTLastSegmentSize = DTSPIFFS_getLastSegmentSize(SPIFFSDTSourceFileLength, SegmentSize);
  SPIFFSbuild_DTFileOpenHeader(SPIFFSDTheader, DTFileOpenHeaderL, filenamesize, SPIFFSDTSourceFileLength, SPIFFSDTSourceFileCRC, SegmentSize);
  SPIFFSLocalPayloadCRC = LoRa.CRCCCITT((uint8_t *) filename, filenamesize, 0xFFFF);

  do
  {
    localattempts++;
#ifdef ENABLEMONITOR
    Monitorport.println(F("Send open remote file request"));
	//Monitorport.print(F("Source file length "));
	//Monitorport.println(SPIFFSDTSourceFileLength);
	//Monitorport.print(F("Source file CRC 0x"));
	//Monitorport.println(SPIFFSDTSourceFileCRC,HEX);
#endif

    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, HIGH);
    }
    SPIFFSTXPacketL = LoRa.transmitDT(SPIFFSDTheader, DTFileOpenHeaderL, (uint8_t *) filename, filenamesize, NetworkID, TXtimeoutmS, TXpower,  WAIT_TX);
    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, LOW);
    }

    SPIFFSTXNetworkID = LoRa.getTXNetworkID(SPIFFSTXPacketL);     //get the networkID appended to packet
    SPIFFSTXArrayCRC = LoRa.getTXPayloadCRC(SPIFFSTXPacketL);     //get the payload CRC appended to packet


#ifdef ENABLEMONITOR
#ifdef DEBUG
    Monitorport.print(F("Send attempt "));
    Monitorport.println(localattempts);
    Monitorport.print(F("SPIFFSTXNetworkID,0x"));
    Monitorport.print(SPIFFSTXNetworkID, HEX);                   //get the NetworkID of the packet just sent, its placed at the packet end
    //Monitorport.print(F(",SPIFFSTXArrayCRC,0x"));
    //Monitorport.println(SPIFFSTXArrayCRC, HEX);                  //get the CRC of the data array just sent, its placed at the packet end
    //Monitorport.println();
#endif
#endif

    if (SPIFFSTXPacketL == 0)                               //if there has been a send and ack error, SPIFFSTXPacketL returns as 0
    {
      Monitorport.println(F("Transmit error"));
    }

    ValidACK = LoRa.waitACKDT(SPIFFSDTheader, DTFileOpenHeaderL, ACKopentimeoutmS);
    SPIFFSRXPacketType = SPIFFSDTheader[0];

    if ((ValidACK > 0) && (SPIFFSRXPacketType == DTFileOpenACK))
    {
#ifdef ENABLEMONITOR
#ifdef DEBUG
      Monitorport.println(F("Valid ACK"));
      //SPIFFSprintPacketHex();
#endif
#endif
    }
    else
    {
      SPIFFSNoAckCount++;
#ifdef ENABLEMONITOR
      Monitorport.println(F("NoACK"));
      //Monitorport.println(SPIFFSNoAckCount);
#ifdef DEBUG
      SPIFFSprintACKdetail();
      Monitorport.print(F("  ACKPacket "));
      //SPIFFSprintPacketHex();
#endif
#endif
      if (SPIFFSNoAckCount > NoAckCountLimit)
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("ERROR NoACK limit reached"));
#endif
        bitSet(SPIFFSDTErrors, SPIFFSNoACKlimit);
        return false;
      }
#ifdef ENABLEMONITOR
      Monitorport.println();
#endif
    }
  }
  while ((ValidACK == 0) && (localattempts < SendAttempts));

  if (localattempts == SendAttempts)
  {
    bitSet(SPIFFSDTErrors, SPIFFSStartTransfer);
    return false;
  }

  return true;
}


bool SPIFFSsendSegments()
{
  // Start the file transfer at segment 0
  SPIFFSDTSegment = 0;
  SPIFFSDTSentSegments = 0;

  dataFile.seek(0);                       //ensure at first position in file

  while (SPIFFSDTSegment < (SPIFFSDTNumberSegments - 1))
  {
#ifdef ENABLEMONITOR
#ifdef DEBUG
    SPIFFSprintSeconds();
#endif
#endif

    if (SPIFFSsendFileSegment(SPIFFSDTSegment, SegmentSize))
    {
      SPIFFSDTSentSegments++;
    }
    else
    {
      bitSet(SPIFFSDTErrors, SPIFFSSendSegment);
      return false;
    }
    delay(FunctionDelaymS);
  };

#ifdef ENABLEMONITOR
  Monitorport.println(F("Last segment"));
#endif

  if (!SPIFFSsendFileSegment(SPIFFSDTSegment, SPIFFSDTLastSegmentSize))
  {
    bitSet(SPIFFSDTErrors, SPIFFSSendSegment);
    return false;
  }

  return true;
}


bool SPIFFSsendFileSegment(uint16_t segnum, uint8_t segmentsize)
{
  // Send file segment as payload in a DT packet

  uint8_t ValidACK;
  uint8_t localattempts = 0;

  DTSPIFFS_readFileSegment(SPIFFSDTdata, segmentsize);
  SPIFFSbuild_DTSegmentHeader(SPIFFSDTheader, DTSegmentWriteHeaderL, segmentsize, segnum);

#ifdef ENABLEMONITOR
#ifdef PRINTSEGMENTNUM
  Monitorport.println(segnum);
#endif


#ifdef DEBUG
  //Monitorport.print(F(" "));
  SPIFFSprintheader(SPIFFSDTheader, DTSegmentWriteHeaderL);
  Monitorport.print(F(" "));
  SPIFFSprintdata(SPIFFSDTdata, segmentsize);                           //print segment size of data array only
#endif
#endif

  do
  {
    localattempts++;

    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, HIGH);
    }

    SPIFFSTXPacketL = LoRa.transmitDT(SPIFFSDTheader, DTSegmentWriteHeaderL, (uint8_t *) SPIFFSDTdata, segmentsize, NetworkID, TXtimeoutmS, TXpower,  WAIT_TX);
    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, LOW);
    }

    if (SPIFFSTXPacketL == 0)                                     //if there has been an error SPIFFSTXPacketL returns as 0
    {
      Monitorport.println(F("Transmit error"));
    }

    ValidACK = LoRa.waitACKDT(SPIFFSDTheader, DTSegmentWriteHeaderL, ACKsegtimeoutmS);
    SPIFFSRXPacketType = SPIFFSDTheader[0];

    if (ValidACK > 0)
    {
      if (SPIFFSRXPacketType == DTSegmentWriteNACK)
      {
        SPIFFSDTSegment = SPIFFSDTheader[4] +  (SPIFFSDTheader[5] << 8);      //load what the segment number should be
        SPIFFSRXHeaderL = SPIFFSDTheader[2];
        DTSPIFFS_seekFileLocation(SPIFFSDTSegment * SegmentSize);
#ifdef ENABLEMONITOR
        Monitorport.println();
        Monitorport.println(F("************************************"));
        Monitorport.print(F("Received restart request at segment "));
        Monitorport.println(SPIFFSDTSegment);
        SPIFFSprintheader(SPIFFSDTheader, SPIFFSRXHeaderL);
        Monitorport.println();
        Monitorport.print(F("Seek to file location "));
        Monitorport.println(SPIFFSDTSegment * SegmentSize);
        Monitorport.println(F("************************************"));
        Monitorport.println();
        Monitorport.flush();
#endif

      }

      //ack is valid, segment was acknowledged if here

      if (SPIFFSRXPacketType == DTStartNACK)
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("Received restart request"));
#endif
        return false;
      }

      if (SPIFFSRXPacketType == DTSegmentWriteACK)
      {
        SPIFFSAckCount++;
        SPIFFSDTSegment++;                  //increase value for next segment
#ifdef ENABLEMONITOR
#ifdef DEBUG
        SPIFFSprintAckBrief();
        //SPIFFSprintAckReception()
#endif
#endif
        return true;
      }
    }
    else
    {
      SPIFFSNoAckCount++;
      Monitorport.println(F("NoACK"));

      if (SPIFFSNoAckCount > NoAckCountLimit)
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("ERROR NoACK limit reached"));
#endif
        bitSet(SPIFFSDTErrors, SPIFFSNoACKlimit);
        return false;
      }
    }
  } while ((ValidACK == 0) && (localattempts < SendAttempts)) ;


  if (localattempts == SendAttempts)
  {
    bitSet(SPIFFSDTErrors, SPIFFSSendSegment);
    return 0;
  }


  return true;
}


bool SPIFFSendFileTransfer(char *filename, uint8_t filenamesize)
{
  // End file transfer, close local file first then remote file

  uint8_t ValidACK;
  uint8_t localattempts = 0;

  DTSPIFFS_closeFile();
  SPIFFSbuild_DTFileCloseHeader(SPIFFSDTheader, DTFileCloseHeaderL, filenamesize, SPIFFSDTSourceFileLength, SPIFFSDTSourceFileCRC, SegmentSize);

  do
  {
    localattempts++;
#ifdef ENABLEMONITOR
    Monitorport.println(F("Send close remote file"));
#endif

    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, HIGH);
    }
    SPIFFSTXPacketL = LoRa.transmitDT(SPIFFSDTheader, DTFileCloseHeaderL, (uint8_t *) filename, filenamesize, NetworkID, TXtimeoutmS, TXpower,  WAIT_TX);
    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, LOW);
    }

    SPIFFSTXNetworkID = LoRa.getTXNetworkID(SPIFFSTXPacketL);
    SPIFFSTXArrayCRC = LoRa.getTXPayloadCRC(SPIFFSTXPacketL);

#ifdef ENABLEMONITOR
#ifdef DEBUG
    Monitorport.print(F("SPIFFSTXNetworkID,0x"));
    Monitorport.print(SPIFFSTXNetworkID, HEX);               //get the NetworkID of the packet just sent, its placed at the packet end
    Monitorport.print(F(",SPIFFSTXArrayCRC,0x"));
    Monitorport.println(SPIFFSTXArrayCRC, HEX);              //get the CRC of the data array just sent, its placed at the packet end
    Monitorport.println();
#endif
#endif

    if (SPIFFSTXPacketL == 0)                           //if there has been a send and ack error, SPIFFSTXPacketL returns as 0
    {
#ifdef ENABLEMONITOR
      Monitorport.println(F("Transmit error"));
#endif
    }

    ValidACK = LoRa.waitACKDT(SPIFFSDTheader, DTFileCloseHeaderL, ACKclosetimeoutmS);
    SPIFFSRXPacketType = SPIFFSDTheader[0];

    if ((ValidACK > 0) && (SPIFFSRXPacketType == DTFileCloseACK))
    {
#ifdef ENABLEMONITOR
#ifdef DEBUG
      //SPIFFSprintPacketHex();
#endif
#endif
    }
    else
    {
      SPIFFSNoAckCount++;
#ifdef ENABLEMONITOR
      Monitorport.println(F("NoACK"));
#endif
      if (SPIFFSNoAckCount > NoAckCountLimit)
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("ERROR NoACK limit reached"));
#endif
        bitSet(SPIFFSDTErrors, SPIFFSNoACKlimit);
        return false;
      }
#ifdef ENABLEMONITOR
#ifdef DEBUG
      Monitorport.println();
      Monitorport.print(F("  ACKPacket "));
      //SPIFFSprintPacketHex();
      Monitorport.println();
#endif
#endif
    }
  }
  while ((ValidACK == 0) && (localattempts < SendAttempts));

  if (localattempts == SendAttempts)
  {
    bitSet(SPIFFSDTErrors, SPIFFSendTransfer);
    return 0;
  }

  return true;
}


void SPIFFSbuild_DTFileOpenHeader(uint8_t *header, uint8_t headersize, uint8_t datalength, uint32_t filelength, uint16_t filecrc, uint8_t segsize)
{
  // This builds the header buffer for the filename to send

  beginarrayRW(header, 0);             //start writing to array at location 0
  arrayWriteUint8(DTFileOpen);         //byte 0, write the packet type
  arrayWriteUint8(SPIFFSDTflags);            //byte 1, SPIFFSDTflags byte
  arrayWriteUint8(headersize);         //byte 2, write length of header
  arrayWriteUint8(datalength);         //byte 3, write length of dataarray
  arrayWriteUint32(filelength);        //byte 4,5,6,7, write the file length
  arrayWriteUint16(filecrc);           //byte 8, 9, write file CRC
  arrayWriteUint8(segsize);            //byte 10, segment size
  arrayWriteUint8(0);                  //byte 11, unused byte
  endarrayRW();
}


void SPIFFSbuild_DTSegmentHeader(uint8_t *header, uint8_t headersize, uint8_t datalen, uint16_t segnum)
{
  // This builds the header buffer for a segment transmit

  beginarrayRW(header, 0);             //start writing to array at location 0
  arrayWriteUint8(DTSegmentWrite);     //write the packet type
  arrayWriteUint8(SPIFFSDTflags);            //SPIFFSDTflags byte
  arrayWriteUint8(headersize);         //write length of header
  arrayWriteUint8(datalen);            //write length of data array
  arrayWriteUint16(segnum);            //write the DTsegment number
  endarrayRW();
}


void SPIFFSbuild_DTFileCloseHeader(uint8_t *header, uint8_t headersize, uint8_t datalength, uint32_t filelength, uint16_t filecrc, uint8_t segsize)
{
  // This builds the header buffer for the filename passed

  beginarrayRW(header, 0);             //start writing to array at location 0
  arrayWriteUint8(DTFileClose);        //byte 0, write the packet type
  arrayWriteUint8(SPIFFSDTflags);            //byte 1, SPIFFSDTflags byte
  arrayWriteUint8(headersize);         //byte 2, write length of header
  arrayWriteUint8(datalength);         //byte 3, write length of dataarray
  arrayWriteUint32(filelength);        //byte 4,5,6,7, write the file length
  arrayWriteUint16(filecrc);           //byte 8, 9, write file CRC
  arrayWriteUint8(segsize);            //byte 10, segment size
  arrayWriteUint8(0);                  //byte 11, unused byte
  endarrayRW();
}


void SPIFFSprintLocalFileDetails()
{
#ifdef ENABLEMONITOR
  Monitorport.print(F("Source file length "));
  Monitorport.print(SPIFFSDTSourceFileLength);
  Monitorport.println(F(" bytes"));
#ifdef ENABLEFILECRC
  Monitorport.print(F("Source file CRC is 0x"));
  Monitorport.println(SPIFFSDTSourceFileCRC, HEX);
#endif
  Monitorport.print(F("Segment Size "));
  Monitorport.println(SegmentSize);
  Monitorport.print(F("Number segments "));
  Monitorport.println(SPIFFSDTNumberSegments);
  Monitorport.print(F("Last segment size "));
  Monitorport.println(SPIFFSDTLastSegmentSize);
#endif
}


void SPIFFSprintSeconds()
{
#ifdef ENABLEMONITOR
  float secs;
  secs = ( (float) millis() / 1000);
  Monitorport.print(secs, 2);
  Monitorport.print(F(" "));
#endif
}


void SPIFFSprintAckBrief()
{
#ifdef ENABLEMONITOR
  SPIFFSPacketRSSI = LoRa.readPacketRSSI();
  Monitorport.print(F(",AckRSSI,"));
  Monitorport.print(SPIFFSPacketRSSI);
  Monitorport.println(F("dBm"));
#endif
}


void SPIFFSprintAckReception()
{
#ifdef ENABLEMONITOR
  SPIFFSPacketRSSI = LoRa.readPacketRSSI();
  SPIFFSPacketSNR = LoRa.readPacketSNR();
  Monitorport.print(F("SPIFFSAckCount,"));
  Monitorport.print(SPIFFSAckCount);
  Monitorport.print(F(",SPIFFSNoAckCount,"));
  Monitorport.print(SPIFFSNoAckCount);
  Monitorport.print(F(",AckRSSI,"));
  Monitorport.print(SPIFFSPacketRSSI);
  Monitorport.print(F("dBm,AckSNR,"));
  Monitorport.print(SPIFFSPacketSNR);
  Monitorport.print(F("dB"));
  Monitorport.println();
#endif
}


void SPIFFSprintACKdetail()
{
#ifdef ENABLEMONITOR
  Monitorport.print(F("ACKDetail"));
  Monitorport.print(F(",RXNetworkID,0x"));
  Monitorport.print(LoRa.getRXNetworkID(SPIFFSRXPacketL), HEX);
  Monitorport.print(F(",RXPayloadCRC,0x"));
  Monitorport.print(LoRa.getRXPayloadCRC(SPIFFSRXPacketL), HEX);
  Monitorport.print(F(",SPIFFSRXPacketL,"));
  Monitorport.print(SPIFFSRXPacketL);
  Monitorport.print(F(" "));
  SPIFFSprintReliableStatus();
  Monitorport.println();
#endif
}


void SPIFFSprintdata(uint8_t *dataarray, uint8_t arraysize)
{
SPIFFSUNUSED(dataarray);           //to prevent a compiler warning
SPIFFSUNUSED(arraysize);           //to prevent a compiler warning
#ifdef ENABLEMONITOR
  Monitorport.print(F("DataBytes,"));
  Monitorport.print(arraysize);
  Monitorport.print(F("  "));
  printarrayHEX((uint8_t *) dataarray, 16);             //There is a lot of data to print so only print first 16 bytes
#endif
}


bool SPIFFSsendDTInfo()
{
  //Send array info packet, for this implmentation its really only the flags in ARDTflags that is sent

  uint8_t ValidACK = 0;
  uint8_t localattempts = 0;

  SPIFFSbuild_DTInfoHeader(SPIFFSDTheader, DTInfoHeaderL, 0);

  do
  {
    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, HIGH);
    }

    localattempts++;
#ifdef ENABLEMONITOR
    Monitorport.print(F("Send DTInfo packet attempt "));
    Monitorport.println(localattempts);
#endif
    SPIFFSTXPacketL = LoRa.transmitDT(SPIFFSDTheader, DTInfoHeaderL, (uint8_t *) SPIFFSDTdata, 0, NetworkID, TXtimeoutmS, TXpower,  WAIT_TX);

    if (SPIFFSTXPacketL == 0)                                         //if there has been an error ARTXPacketL returns as 0
    {
#ifdef ENABLEMONITOR
      Monitorport.println(F("Transmit error"));
#endif
      continue;
    }

    ValidACK = LoRa.waitACKDT(SPIFFSDTheader, DTInfoHeaderL, ACKsegtimeoutmS);

    if (ValidACK > 0)
    {
      //ack is a valid relaible packet
      SPIFFSRXPacketType = SPIFFSDTheader[0];
#ifdef ENABLEMONITOR
      Monitorport.print(F("ACK Packet type 0x"));
      Monitorport.println(SPIFFSRXPacketType, HEX);
#endif

      if (SPIFFSRXPacketType == DTInfoACK)
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("DTInfoACK received"));
#endif
        SPIFFSAckCount++;
        SPIFFSRXPacketType = SPIFFSDTheader[0];
        return true;
      }
      else
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("DTInfoACK not received"));
#endif
        return false;
      }

    }
    else
    {
      SPIFFSNoAckCount++;
#ifdef ENABLEMONITOR
      Monitorport.println(F("No valid ACK received "));
#endif

      if (SPIFFSNoAckCount > NoAckCountLimit)
      {
#ifdef ENABLEMONITOR
        Monitorport.println(F("ERROR NoACK limit reached"));
#endif
        return false;
      }
    }
    delay(PacketDelaymS);
  }
  while ((ValidACK == 0) && (localattempts < SendAttempts));

  if (localattempts == SendAttempts)
  {
    bitSet(SPIFFSDTErrors, SPIFFSSendPacket);
    return false;
  }
  return true;
}


void SPIFFSbuild_DTInfoHeader(uint8_t *header, uint8_t headersize, uint8_t datalen)
{
  //This builds the header buffer for a info only header, faults problems etc

  beginarrayRW(header, 0);             //start writing to array at location 0
  arrayWriteUint8(DTInfo);             //write the packet type
  arrayWriteUint8(SPIFFSDTflags);          //ARDTflags byte
  arrayWriteUint8(headersize);         //write length of header
  arrayWriteUint8(datalen);            //write length of data array
  arrayWriteUint8(0);                  //unused
  arrayWriteUint8(0);                  //unused
  arrayWriteUint8(0);                  //unused
  arrayWriteUint8(0);                  //unused
  endarrayRW();
}








//************************************************
//Receiver mode  functions
//************************************************


bool SPIFFSreceiveaPacketDT()
{
  // Receive Data transfer packets

  SPIFFSRXPacketType = 0;
  SPIFFSRXPacketL = LoRa.receiveDT(SPIFFSDTheader, HeaderSizeMax, (uint8_t *) SPIFFSDTdata, DataSizeMax, NetworkID, RXtimeoutmS, WAIT_RX);

  if (SPIFFSDTLED >= 0)
  {
    digitalWrite(SPIFFSDTLED, HIGH);
  }

#ifdef DEBUG
  SPIFFSprintSeconds();
#endif

  if (SPIFFSRXPacketL > 0)
  {
    //if the LT.receiveDT() returns a value > 0 for SPIFFSRXPacketL then packet was received OK
    //then only action payload if destinationNode = thisNode
    SPIFFSreadHeaderDT();                      //get the basic header details into global variables SPIFFSRXPacketType etc
    SPIFFSprocessPacket(SPIFFSRXPacketType);         //process and act on the packet
    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, LOW);
    }
    return true;
  }
  else
  {
    //if the LoRa.receiveDT() function detects an error RXOK is 0
    uint16_t IRQStatus = LoRa.readIrqStatus();
    
    if (IRQStatus & IRQ_RX_TIMEOUT)
    {
    #ifdef ENABLEMONITOR
    Monitorport.println(F("RX Timeout")); 
    #endif
    }
    else
    { 
    SPIFFSRXErrors++;
#ifdef ENABLEMONITOR
#ifdef DEBUG
    Monitorport.print(F("PacketError"));
    SPIFFSprintPacketDetails();
    SPIFFSprintReliableStatus();
    Monitorport.println();
#endif
#endif

    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, LOW);
    }
    
  }
 }
 return false;
 
}


void SPIFFSreadHeaderDT()
{
  // The first 6 bytes of the header contain the important stuff, so load it up
  // so we can decide what to do next.
  beginarrayRW(SPIFFSDTheader, 0);                      //start buffer read at location 0
  SPIFFSRXPacketType = arrayReadUint8();                //load the packet type
  SPIFFSRXFlags = arrayReadUint8();                     //SPIFFSDTflags byte
  SPIFFSRXHeaderL = arrayReadUint8();                   //load the header length
  SPIFFSRXDataarrayL = arrayReadUint8();                //load the datalength
  SPIFFSDTSegment = arrayReadUint16();                  //load the segment number
}


bool SPIFFSprocessPacket(uint8_t packettype)
{
  // Decide what to do with an incoming packet

  if (packettype == DTSegmentWrite)
  {
    SPIFFSprocessSegmentWrite();
    return true;
  }

  if (packettype == DTFileOpen)
  {
    SPIFFSprocessFileOpen(SPIFFSDTdata, SPIFFSRXDataarrayL);
    return true;
  }

  if (packettype == DTFileClose)
  {
    SPIFFSprocessFileClose();
    return true;
  }

  return true;
}


void SPIFFSprintPacketDetails()
{
#ifdef ENABLEMONITOR
  SPIFFSPacketRSSI = LoRa.readPacketRSSI();
  SPIFFSPacketSNR = LoRa.readPacketSNR();
  Monitorport.print(F(" RSSI,"));
  Monitorport.print(SPIFFSPacketRSSI);
  Monitorport.print(F("dBm"));

#ifdef DEBUG
  Monitorport.print(F(",SNR,"));
  Monitorport.print(SPIFFSPacketSNR);
  Monitorport.print(F("dBm,RXOKCount,"));
  Monitorport.print(SPIFFSDTReceivedSegments);
  Monitorport.print(F(",RXErrs,"));
  Monitorport.print(SPIFFSRXErrors);
  Monitorport.print(F(" RX"));
  SPIFFSprintheader(SPIFFSDTheader, SPIFFSRXHeaderL);
#endif
#endif
}


bool SPIFFSprocessSegmentWrite()
{
  // There is a request to write a segment to file on receiver
  // checks that the sequence of segment writes is correct

  if (!SPIFFSDTFileOpened)
  {
    //something is wrong, have received a request to write a segment but there is no file opened
    //need to reject the segment write with a restart NACK
#ifdef ENABLEMONITOR
    Monitorport.println();
    Monitorport.println(F("***************************************************"));
    Monitorport.println(F("Error - Segment write with no file open - send NACK"));
    Monitorport.println(F("***************************************************"));
    Monitorport.println();
#endif
    SPIFFSDTheader[0] = DTStartNACK;
    delay(ACKdelaymS);
    delay(DuplicatedelaymS);

    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, HIGH);
    }
    LoRa.sendACKDT(SPIFFSDTheader, DTStartHeaderL, TXpower);
    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, LOW);
    }
    return false;
  }

  if (SPIFFSDTSegment == SPIFFSDTSegmentNext)
  {
    DTSPIFFS_writeSegmentFile(SPIFFSDTdata, SPIFFSRXDataarrayL);
    //DTSPIFFS_fileFlush();

#ifdef ENABLEMONITOR
#ifdef PRINTSEGMENTNUM
    //Monitorport.print(F("Segment,"));
    Monitorport.println(SPIFFSDTSegment);
#endif

#ifdef DEBUG
    Monitorport.print(F("Bytes,"));
    Monitorport.print(SPIFFSRXDataarrayL);
    SPIFFSprintPacketRSSI();
    Monitorport.println(F(" SendACK"));
#endif
#endif
    SPIFFSDTheader[0] = DTSegmentWriteACK;
    delay(ACKdelaymS);

    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, HIGH);
    }
    LoRa.sendACKDT(SPIFFSDTheader, DTSegmentWriteHeaderL, TXpower);

    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, LOW);
    }
    SPIFFSDTReceivedSegments++;
    SPIFFSDTSegmentLast = SPIFFSDTSegment;                  //so we can tell if sequece has been received twice
    SPIFFSDTSegmentNext = SPIFFSDTSegment + 1;
    return true;
  }

  if (SPIFFSDTSegment == SPIFFSDTSegmentLast)
  {
#ifdef ENABLEMONITOR
    Monitorport.print(F("ERROR segment "));
    Monitorport.print(SPIFFSDTSegment);
    Monitorport.println(F(" already received "));
    delay(DuplicatedelaymS);
#ifdef DEBUG
    SPIFFSprintPacketDetails();
    SPIFFSprintPacketRSSI();
#endif
#endif
    SPIFFSDTheader[0] = DTSegmentWriteACK;
    delay(ACKdelaymS);

    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, HIGH);
    }
    LoRa.sendACKDT(SPIFFSDTheader, DTSegmentWriteHeaderL, TXpower);

    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, LOW);
    }
    return true;
  }

  if (SPIFFSDTSegment != SPIFFSDTSegmentNext )
  {
    SPIFFSDTheader[0] = DTSegmentWriteNACK;
    SPIFFSDTheader[4] = lowByte(SPIFFSDTSegmentNext);
    SPIFFSDTheader[5] = highByte(SPIFFSDTSegmentNext);
    delay(ACKdelaymS);
    delay(DuplicatedelaymS);                   //add an extra delay here to stop repeated segment sends

#ifdef ENABLEMONITOR
    Monitorport.print(F(" ERROR Received Segment "));
    Monitorport.print(SPIFFSDTSegment);
    Monitorport.print(F(" expected "));
    Monitorport.print(SPIFFSDTSegmentNext);
    Monitorport.print(F(" "));

#ifdef DEBUG
    SPIFFSprintPacketDetails();
    SPIFFSprintPacketRSSI();
#endif

    Monitorport.print(F(" Send NACK for segment "));
    Monitorport.print(SPIFFSDTSegmentNext);
    Monitorport.println();
    Monitorport.println();
    Monitorport.println(F("*****************************************"));
    Monitorport.print(F("Transmit restart request for segment "));
    Monitorport.println(SPIFFSDTSegmentNext);
    SPIFFSprintheader(SPIFFSDTheader, SPIFFSRXHeaderL);
    Monitorport.println();
    Monitorport.println(F("*****************************************"));
    Monitorport.println();
    Monitorport.flush();
#endif

    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, HIGH);
    }
    LoRa.sendACKDT(SPIFFSDTheader, DTSegmentWriteHeaderL, TXpower);
    if (SPIFFSDTLED >= 0)
    {
      digitalWrite(SPIFFSDTLED, LOW);
    }
    return false;
  }

  return true;
}


bool SPIFFSprocessFileOpen(uint8_t *filename, uint8_t filenamesize)
{
  // There is a request to open local file on receiver

  SPIFFSDTDestinationFileCRC = 0;                          //CRC of complete file received
  SPIFFSDTDestinationFileLength =0;                        //length of file written on the destination\receiver
  
  beginarrayRW(SPIFFSDTheader, 4);                         //start buffer read at location 4
  SPIFFSDTSourceFileLength = arrayReadUint32();            //load the file length of the remote file being sent
  SPIFFSDTSourceFileCRC = arrayReadUint16();               //load the CRC of the source file being sent
  memset(SPIFFSDTfilenamebuff, 0, Maxfilenamesize);        //clear SPIFFSDTfilenamebuff to all 0s
  memcpy(SPIFFSDTfilenamebuff, filename, filenamesize);    //copy received SPIFFSDTdata into SPIFFSDTfilenamebuff
  Monitorport.print((char*) SPIFFSDTfilenamebuff);
  Monitorport.print(F(" SPIFFS File Open request"));
  Monitorport.println();
  SPIFFSprintSourceFileDetails();

  if bitRead(SPIFFSRXFlags, SPIFFSNoFileSave)
  {
#ifdef ENABLEMONITOR
    Monitorport.println(F("Remote did not save file to SPIFFS"));
#endif
  }

  if (DTSPIFFS_openNewFileWrite(SPIFFSDTfilenamebuff))      //open file for write at beginning, delete if it exists
  {
#ifdef ENABLEMONITOR
    Monitorport.print((char*) SPIFFSDTfilenamebuff);
    Monitorport.println(F(" DT File Opened OK"));
    Monitorport.println(F("Waiting transfer"));
#endif
    SPIFFSDTSegmentNext = 0;                            //since file is opened the next sequence should be the first
    SPIFFSDTFileOpened = true;
    SPIFFSDTStartmS = millis();
  }
  else
  {
#ifdef ENABLEMONITOR
    Monitorport.print((char*) SPIFFSDTfilenamebuff);
    Monitorport.println(F(" File Open fail"));
#endif
    SPIFFSDTFileOpened = false;
    return false;
  }

  SPIFFSDTStartmS = millis();
  delay(ACKdelaymS);
#ifdef ENABLEMONITOR
#ifdef DEBUG
  Monitorport.println(F("Sending ACK"));
#endif
#endif
  SPIFFSDTheader[0] = DTFileOpenACK;                    //set the ACK packet type

  if (SPIFFSDTLED >= 0)
  {
    digitalWrite(SPIFFSDTLED, HIGH);
  }
  LoRa.sendACKDT(SPIFFSDTheader, DTFileOpenHeaderL, TXpower);
  if (SPIFFSDTLED >= 0)
  {
    digitalWrite(SPIFFSDTLED, LOW);
  }
  SPIFFSDTSegmentNext = 0;                               //after file open, segment 0 is next

  return true;
}


bool SPIFFSprocessFileClose()
{
  // There is a request to close a file on SPIFFS of receiver

#ifdef ENABLEMONITOR
  Monitorport.print((char*) SPIFFSDTfilenamebuff);
  Monitorport.println(F(" File close request"));
#endif

  if (SPIFFSDTFileOpened)                                     //check if file has been opened, close it if it is
  {
    if (SPIFFS.exists(SPIFFSDTfilenamebuff))                      //check if file exists
    {
      DTSPIFFS_closeFile();

#ifdef ENABLEMONITOR
      Monitorport.print(F("Transfer time "));
      Monitorport.print(millis() - SPIFFSDTStartmS);
      Monitorport.print(F("mS"));
      Monitorport.println();
      Monitorport.println(F("File closed"));
#endif
      SPIFFSDTFileOpened = false;
      SPIFFSDTDestinationFileLength = DTSPIFFS_openFileRead(SPIFFSDTfilenamebuff);
#ifdef ENABLEFILECRC
      SPIFFSDTDestinationFileCRC = DTSPIFFS_fileCRCCCITT(SPIFFSDTDestinationFileLength);
#endif
      beginarrayRW(SPIFFSDTheader, 4);                       //start writing to array at location 12
      arrayWriteUint32(SPIFFSDTDestinationFileLength);       //write file length of file just written just written to ACK header
      arrayWriteUint16(SPIFFSDTDestinationFileCRC);          //write CRC of file just written to ACK header
#ifdef ENABLEMONITOR
      SPIFFSprintDestinationFileDetails();
#endif
    }
  }
  else
  {
#ifdef ENABLEMONITOR
    Monitorport.println(F("File already closed"));
#endif
    delay(DuplicatedelaymS);
  }

  delay(ACKdelaymS);
#ifdef ENABLEMONITOR
#ifdef DEBUG
  Monitorport.println(F("Sending ACK"));
#endif
#endif
  SPIFFSDTheader[0] = DTFileCloseACK;

  if (SPIFFSDTLED >= 0)
  {
    digitalWrite(SPIFFSDTLED, HIGH);
  }
  LoRa.sendACKDT(SPIFFSDTheader, DTFileCloseHeaderL, TXpower);
  if (SPIFFSDTLED >= 0)
  {
    digitalWrite(SPIFFSDTLED, LOW);
  }
#ifdef ENABLEMONITOR
  Monitorport.println();
#endif

  return true;
}


void SPIFFSprintPacketRSSI()
{
#ifdef ENABLEMONITOR
  SPIFFSPacketRSSI = LoRa.readPacketRSSI();
  Monitorport.print(F(" RSSI,"));
  Monitorport.print(SPIFFSPacketRSSI);
  Monitorport.print(F("dBm"));
#endif
}


void SPIFFSprintSourceFileDetails()
{
  //Monitorport.print(SPIFFSDTfilenamebuff);
#ifdef ENABLEMONITOR
  Monitorport.print(F("Source file length is "));
  Monitorport.print(SPIFFSDTSourceFileLength);
  Monitorport.println(F(" bytes"));
#ifdef ENABLEFILECRC
  Monitorport.print(F("Source file CRC is 0x"));
  Monitorport.println(SPIFFSDTSourceFileCRC, HEX);
#endif
#endif
}


void SPIFFSprintDestinationFileDetails()
{
#ifdef ENABLEMONITOR
  Monitorport.print(F("Destination file length "));
  Monitorport.print(SPIFFSDTDestinationFileLength);
  Monitorport.println(F(" bytes"));
  if (SPIFFSDTDestinationFileLength != SPIFFSDTSourceFileLength)
  {
    Monitorport.println(F("ERROR - file lengths do not match"));
  }
  else
  {
    Monitorport.println(F("File lengths match"));
  }

#ifdef ENABLEFILECRC
  Monitorport.print(F("Destination file CRC is 0x"));
  Monitorport.println(SPIFFSDTDestinationFileCRC, HEX);
  if (SPIFFSDTDestinationFileCRC != SPIFFSDTSourceFileCRC)
  {
    Monitorport.println(F("ERROR - file CRCs do not match"));
  }
  else
  {
    Monitorport.println(F("File CRCs match"));
  }
#endif
#endif
}

//************************************************
//Common functions
//************************************************

void SPIFFSsetLED(int8_t pinnumber)
{
  if (pinnumber >= 0)
  {
    SPIFFSDTLED = pinnumber;
    pinMode(pinnumber, OUTPUT);
  }
}


void SPIFFSprintheader(uint8_t *header, uint8_t headersize)
{
SPIFFSUNUSED(header);               //to prevent a compiler warning
SPIFFSUNUSED(headersize);           //to prevent a compiler warning

#ifdef ENABLEMONITOR
  Monitorport.print(F("HeaderBytes,"));
  Monitorport.print(headersize);
  Monitorport.print(F(" "));
  printarrayHEX(header, headersize);
#endif
}


void printArrayHEX(uint8_t *buff, uint32_t len)
{
SPIFFSUNUSED(buff);           //to prevent a compiler warning
SPIFFSUNUSED(len);           //to prevent a compiler warning

#ifdef ENABLEMONITOR
  uint8_t index, buffdata;
  for (index = 0; index < len; index++)
  {
    buffdata = buff[index];
    if (buffdata < 16)
    {
      Monitorport.print(F("0"));
    }
    Monitorport.print(buffdata, HEX);
    Monitorport.print(F(" "));
  }
#endif
}


void SPIFFSprintReliableStatus()
{
  
#ifdef ENABLEMONITOR
  
  uint8_t reliableErrors = LoRa.readReliableErrors();
  uint8_t reliableFlags = LoRa.readReliableFlags();
  
  if (bitRead(reliableErrors, ReliableCRCError))
  {
    Monitorport.print(F(",ReliableCRCError"));
  }

  if (bitRead(reliableErrors, ReliableIDError))
  {
    Monitorport.print(F(",ReliableIDError"));
  }

  if (bitRead(reliableErrors, ReliableSizeError))
  {
    Monitorport.print(F(",ReliableSizeError"));
  }

  if (bitRead(reliableErrors, ReliableACKError))
  {
    Monitorport.print(F(",NoReliableACK"));
  }

  if (bitRead(reliableErrors, ReliableTimeout))
  {
    Monitorport.print(F(",ReliableTimeout"));
  }

  if (bitRead(reliableFlags, ReliableACKSent))
  {
    Monitorport.print(F(",ACKsent"));
  }

  if (bitRead(reliableFlags, ReliableACKReceived))
  {
    Monitorport.print(F(",ACKreceived"));
  }
#endif
}
